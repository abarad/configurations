export ZSH_THEME="sonicradish"

alias c="clear"

plugins=(
    git
    zsh-autosuggestions
    zsh-syntax-highlighting
    zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
