## Programs
- VSCode
- Pycharm
- Docker
- Chrome
- Discord
- VLC
- Oh-My-Zsh
- Tmux


## Python Modules
- IPython

## VSCode Extensions
- Material Theme
- Material Theme Icons
- Auto Close Tag
- Bracket Pair Colorizer
- Docker
- Code Runner
- Github Copilot

## Chrome Extensions
- [Vimium](https://chrome.google.com/webstore/detail/vimium/dbepggeogbaibhgnhhndojpepiihcmeb?hl=en)
- [JSON viewer](https://chrome.google.com/webstore/detail/json-viewer/gbmdgpbipfallnflgajpaliibnhdgobh/related?hl=en)
- [AdBlock](https://chrome.google.com/webstore/detail/adblock-%E2%80%94-best-ad-blocker/gighmmpiobklfepjocnamgkkbiglidom?hl=en)
- [Dark Reader](https://chrome.google.com/webstore/detail/dark-reader/eimadpbcbfnmbkopoojfekhnkhdbieeh?hl=en)

## Linux Tweaks
- Make terminal partially transparent.
- GNOME icons package `Zafiro-icons-light`
- GNOME theme `Arc-dark`
- GNOME tweaks curser `Breeze_cursors`

